function Menu1v4() { 
    document.getElementById("btnv4_1").className = "mostrar-v4-1";
    document.getElementById("btnv4_3").className = "esconder-v4-3";
    document.getElementById("btnv4_6").className = "esconder-v4-6";
    
    document.getElementById("btnv4_1a").className = "boton1-v4v";
    document.getElementById("btnv4_3a").className = "boton1-v4";
    document.getElementById("btnv4_6a").className = "boton1-v4";
}


function Menu3v4() { 
    document.getElementById("btnv4_1").className = "esconder-v4-1";
    document.getElementById("btnv4_3").className = "mostrar3";
    document.getElementById("btnv4_6").className = "esconder-v4-6";

    document.getElementById("btnv4_1a").className = "boton1-v4";
    document.getElementById("btnv4_3a").className = "boton1-v4v";
    document.getElementById("btnv4_6a").className = "boton1-v4";
}


function Menu6v4() { 
    document.getElementById("btnv4_1").className = "esconder-v4-1";
    document.getElementById("btnv4_3").className = "esconder-v4-3";
    document.getElementById("btnv4_6").className = "mostrar6";

    document.getElementById("btnv4_1a").className = "boton1-v4";
    document.getElementById("btnv4_3a").className = "boton1-v4";
    document.getElementById("btnv4_6a").className = "boton1-v4v";
}

/* Pagina 2 */

function Menu1_2v4() { 
    document.getElementById("btnv4_2_1").className = "mostrar1";
    document.getElementById("btnv4_2_2").className = "esconder-v4-2";
    document.getElementById("btnv4_2_3").className = "esconder-v4-3";
    document.getElementById("btnv4_2_4").className = "esconder-v4-4";
    document.getElementById("btnv4_2_5").className = "esconder-v4-5";
    document.getElementById("btnv4_2_6").className = "esconder-v4-6";
    
    document.getElementById("btnv4_2_1a").className = "boton1-2-v4v";
    document.getElementById("btnv4_2_2a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_3a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_4a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_5a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_6a").className = "boton1-2-v4";
}

function Menu2_2v4() {
    document.getElementById("btnv4_2_1").className = "esconder-v4-1";
    document.getElementById("btnv4_2_2").className = "mostrar2";
    document.getElementById("btnv4_2_3").className = "esconder-v4-3";
    document.getElementById("btnv4_2_4").className = "esconder-v4-4";
    document.getElementById("btnv4_2_5").className = "esconder-v4-5";
    document.getElementById("btnv4_2_6").className = "esconder-v4-6";

    document.getElementById("btnv4_2_1a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_2a").className = "boton1-2-v4v";
    document.getElementById("btnv4_2_3a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_4a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_5a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_6a").className = "boton1-2-v4";
}

// Define funciones para los botones 3, 4, 5 y 6 de manera similar
function Menu3_2v4() { 
    document.getElementById("btnv4_2_1").className = "esconder-v4-1";
    document.getElementById("btnv4_2_2").className = "esconder-v4-2";
    document.getElementById("btnv4_2_3").className = "mostrar3";
    document.getElementById("btnv4_2_4").className = "esconder-v4-4";
    document.getElementById("btnv4_2_5").className = "esconder-v4-5";
    document.getElementById("btnv4_2_6").className = "esconder-v4-6";

    // Ajusta las clases de los botones para reflejar el estado actual
    document.getElementById("btnv4_2_1a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_2a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_3a").className = "boton1-2-v4v";
    document.getElementById("btnv4_2_4a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_5a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_6a").className = "boton1-2-v4";
}

function Menu4_2v4() { 
    document.getElementById("btnv4_2_1").className = "esconder-v4-1";
    document.getElementById("btnv4_2_2").className = "esconder-v4-2";
    document.getElementById("btnv4_2_3").className = "esconder-v4-3";
    document.getElementById("btnv4_2_5").className = "esconder-v4-5";
    document.getElementById("btnv4_2_6").className = "esconder-v4-6";

    // Muestra el contenido correspondiente al botón 4
    document.getElementById("btnv4_2_4").className = "mostrar4";

    // Ajusta las clases de los botones para reflejar el estado actual
    document.getElementById("btnv4_2_1a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_2a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_3a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_4a").className = "boton1-2-v4v";
    document.getElementById("btnv4_2_5a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_6a").className = "boton1-2-v4";
    
}

function Menu5_2v4() { 
    document.getElementById("btnv4_2_1").className = "esconder-v4-1";
    document.getElementById("btnv4_2_2").className = "esconder-v4-2";
    document.getElementById("btnv4_2_3").className = "esconder-v4-3";
    document.getElementById("btnv4_2_4").className = "esconder-v4-4";
    document.getElementById("btnv4_2_6").className = "esconder-v4-6";

    // Muestra el contenido correspondiente al botón 5
    document.getElementById("btnv4_2_5").className = "mostrar5";

    // Ajusta las clases de los botones para reflejar el estado actual
    document.getElementById("btnv4_2_1a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_2a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_3a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_4a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_5a").className = "boton1-2-v4v";
    document.getElementById("btnv4_2_6a").className = "boton1-2-v4";
}

function Menu6_2v4() { 
    document.getElementById("btnv4_2_1").className = "esconder-v4-1";
    document.getElementById("btnv4_2_2").className = "esconder-v4-2";
    document.getElementById("btnv4_2_3").className = "esconder-v4-3";
    document.getElementById("btnv4_2_4").className = "esconder-v4-4";
    document.getElementById("btnv4_2_5").className = "esconder-v4-5";

    // Muestra el contenido correspondiente al botón 6
    document.getElementById("btnv4_2_6").className = "mostrar6";

    // Ajusta las clases de los botones para reflejar el estado actual
    document.getElementById("btnv4_2_1a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_2a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_3a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_4a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_5a").className = "boton1-2-v4";
    document.getElementById("btnv4_2_6a").className = "boton1-2-v4v";
}

